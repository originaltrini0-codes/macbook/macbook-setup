#! /usr/bin/env bash

set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

# Introduce global variables
# shellcheck disable=SC1091
source "$(dirname $BASH_SOURCE[0])/.vars"

# Define functions
function is-cask-installed() {
    if [[ $1 ]] && brew list --casks | grep $1 --quiet
    then
        return 0
    fi

    return 1
}


function is-formula-installed() {
    if [[ $1 ]] && brew list --formulae | grep $1 --quiet
    then
        return 0
    fi

    return 1
}

function is-store-app-installed() {
    if [[ $1 ]] && mas list | grep $1 --quiet
    then
        return 0
    fi

    return 1
}


function print-fault() {
    printf "%s\n" "$1"
    exit 1
}


function print-info() {
    printf "%b\n" "$1"
}


function print-introduction() {
    clear
    file_path="$_SETUP_BASE_DIRECTORY/ascii-intro"
    printf "%b\n" "$(cat $file_path)"
}


function print-warning() {
    print "WARNING: %b\n" "$1"
}


function print-header {
    local line=""
    local message="${1}"
    local recurrence=${#message}

    for ((i=0; i<recurrence; i++))
    do
        line+="-"
    done

    printf "\n\n%s\n%s\n%s\n" ${line} "${message}" ${line}
}


function sudo-creds-clear() {
    sudo --reset-timestamp
}


function sudo-creds-challenge() {
    if [[ $EUID -ne 0 ]]
    then
        sudo-creds-clear

        if ! sudo true
        then
            print-fault "Unable to validate your sudo credentials.  Exiting"
            exit 1
        fi
    fi
}


function sudo-creds-extend() {
    sudo --validate
}


print-introduction
sleep 2
printf "\n\n"
print-info "Enter your sudo password below"
sudo-creds-challenge


# Find *.sh files in the setup directory that has the "user" executable bit turned on
# Sort and loop to source the files
find "${_SETUP_SCRIPTS_PATH-}" -perm -u=x -type f -name "${_SETUP_SCRIPTS_EXT-}" -print0 | xargs -0 -I {} | sort | while IFS= read -r _file; do
    # shellcheck disable=SC1090
    source "$_file"
    sudo-creds-extend
done

sudo-creds-clear