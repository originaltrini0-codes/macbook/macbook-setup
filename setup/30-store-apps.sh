#! /usr/bin/env bash

# Installing apps
print-header "Installing MacOS store applications"

while IFS= read -r store_app
do
    if is-store-app-installed "$store_app"
    then
        print-info "Store app ${store_app} is already installed."
    else
        print-info "Installing Store app: ${store_app}"
        mas install "$store_app"
    fi
done < <(yq .store-apps[] < "$_SETUP_USER_APPS_PREFS_FILE")