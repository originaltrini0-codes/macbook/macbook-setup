#! /usr/bin/env bash

# Installing apps
print-header "Installing GUI applications"

while IFS= read -r cask
do
    if is-cask-installed "$cask"
    then
        print-info "Cask ${cask} is already installed."
        # brew upgrade --quiet --cask "$cask" >/dev/null
    else
        print-info "Installing Cask: ${cask}"
        brew install --quiet --cask "$cask" >/dev/null
    fi
done < <(yq .casks[] < "$_SETUP_USER_APPS_PREFS_FILE")