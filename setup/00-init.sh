#! /usr/bin/env bash

print-header "Installing Homebrew"

# Is Homebrew installed
if [[ -z $(which brew) ]]
then
    print-info "Installing Homebrew..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    print-info "Homebrew is already installed.  Applying any updates..."
    brew update --auto-update --quiet
fi

# Add Homebrew path to zsh
if ! fgrep 'eval "$(/opt/homebrew/bin/brew shellenvs)"' "$HOME/.zprofile"
then
    print-info "Updating zsh \$PATH environment variable"
    (echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> "$HOME/.zprofile"
    eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# Install yq (YAML processor) as a pre-requisite
if ! is-formula-installed "yq"
then
    print-info "Installing requirement: yq"
    brew install --quiet --formula yq >/dev/null
fi