#! /usr/bin/env bash

# Installing tools
print-header "Installing Terminal tools"

while IFS= read -r formula
do
    if is-formula-installed "$formula"
    then
        print-info "Formula ${formula} is already installed."
        # brew upgrade --quiet --formula "$formula" >/dev/null
    else
        print-info "Installing formula ${formula}"
        NONINTERACTIVE=1 brew install --quiet --formula "$formula" >/dev/null
    fi
done < <(yq .formulae[] < "$_SETUP_USER_APPS_PREFS_FILE")

#### BASH configuration
# Add Homebrew bash to /etc/shells
if ! grep --quiet "/opt/homebrew/bin/bash" /etc/shells
then
    sudo sh -c 'echo /opt/homebrew/bin/bash >> /etc/shells'
fi

# Maybe ask the question if the default shell should be bash
#     chsh -s /opt/homebrew/bin/bash

# BASH_COMPLETION_CONFIG='[[ -r "/opt/homebrew/etc/profile.d/bash_completion.sh" ]] && . "/opt/homebrew/etc/profile.d/bash_completion.sh"'

# if [[ $(grep -c "${BASH_COMPLETION_CONFIG}" "${HOME}/.bash_profile") == 0 ]]
# then
#     cat <<- EOF >> "${HOME}"/.bash_profile

#     # Required for bash-completion@2
#     [[ -r "/opt/homebrew/etc/profile.d/bash_completion.sh" ]] && . "/opt/homebrew/etc/profile.d/bash_completion.sh"
# EOF
# fi