#! /usr/bin/env bash

## MacOS Preferences
# Close any open System Preferences panes, to prevent them from overriding
# settings we’re about to change
osascript -e 'tell application "System Preferences" to quit'

# Finder
defaults write NSGlobalDomain "AppleShowAllExtensions" -bool "true"         # Show file extensions
defaults write com.apple.finder "AppleShowAllFiles" -bool "true"            # Show hidden files
defaults write com.apple.finder "ShowPathbar" -bool "true"                  # Show the path bar
defaults write com.apple.finder "FXPreferredViewStyle" -string "Nlsv"       # Use list view
defaults write com.apple.finder "FXDefaultSearchScope" -string "SCcf"       # Search the current folder
defaults write com.apple.finder "_FXSortFoldersFirst" -bool "true"          # List folders first before files
killall Finder

# Dock
defaults write com.apple.dock "orientation" -string "bottom"                # Even though it is the default, set the dock to the bottom
defaults write com.apple.dock "autohide" -bool "true"                       # Auto hide the dock
defaults write com.apple.dock "show-recents" -bool "false"                  # Do not display recent apps in the dock
killall Dock

# Trackpad
defaults write 'Apple Global Domain' com.apple.trackpad.forceClick -bool false      # Disable trackpad force click to prevent the dictionary popup

# Keyboard (Idea from r/MacOS)
## https://www.reddit.com/r/MacOS/comments/pz9vnu/behavior_of_the_home_and_end_keys/
keybind_file="$HOME/Library/KeyBindings/DefaultKeyBinding.dict"
if [[ ! -f $keybind_file ]]
then
    if [[ ! -d $(dirname $keybind_file) ]]
    then
        mkdir $(dirname $keybind_file)
    fi

    cat << 'EOF' > $keybind_file
{
    "\UF729" = "moveToBeginningOfLine:";
    "\UF72B" = "moveToEndOfLine:";
    "$\UF729" = moveToBeginningOfLineAndModifySelection:; // shift-home
    "$\UF72B" = moveToEndOfLineAndModifySelection:; // shift-end
    "^\UF729" = moveToBeginningOfDocument:; // ctrl-home
    "^\UF72B" = moveToEndOfDocument:; // ctrl-end
    "^$\UF729" = moveToBeginningOfDocumentAndModifySelection:; // ctrl-shift-home
    "^$\UF72B" = moveToEndOfDocumentAndModifySelection:; // ctrl-shift-end
}
EOF
fi

## Microsoft Edge Preferences
defaults write com.microsoft.Edge SwitchIntranetSitesToWorkProfile -bool FALSE      # Disable automatic profile switching
defaults write com.microsoft.Edge HubsSidebarEnabled -bool FALSE                    # Disable Edge/Copilot 
