#! /usr/bin/env bash

# Upgrade installed packages/applications
print-header "Upgrading installed packages and applications"
brew upgrade

# Cleanup disk space as much as possible
print-header "Wrapping up"
print-info "Clearing cache storage"
brew cleanup --prune=all --quiet >/dev/null


if [[ "${_SETUP_RESTART_TERMINAL-}" -ne 0 ]]
then
    print-info "Quit/Restart all terminal sessions for changes to take effect"
fi