# Gerard Samuel: macbook-setup

## [RETIRED]: I am retiring this script and will move to using Ansible [instead](https://gitlab.com/originaltrini0-codes/workstation/desktop-setup).

Bash scripts to setup tools/apps on a new macbook

## Requirements
* Macbook
    * Developed on Apple's SoC
* MacOS
    * Developed on version 14 (Sonoma)
* Local admin role
* Internet access

## Defining tools/applications to install
Edit setup.yaml to list applications and or tools to install.
The tool yq is installed by default and is not listed in the setup.yaml file.

## How to execute
From a command prompt, execute the following:

```
curl --fail --show-error --silent --location \
https://gitlab.com/originaltrini0-codes/macbook/macbook-setup/-/archive/main/macbook-setup-main.tar.gz \
| tar --extract --keep-old-files --gzip --directory $HOME/ \
&& /bin/bash -c '$HOME/macbook-setup-main/bootstrap.sh'
```

## TODO:
- "Terminal" rights under:
    - System Settings -> Privacy & Security -> App Management
- "Logi Options Daemon" rights under:
    - System Settings -> Privacy & Security -> Accessibility
    - System Settings -> Privacy & Security -> Bluetooth
- "Microsoft Edge" rights under:
    - System Settings -> Privacy & Security -> Bluetooth
    - System Settings -> Privacy & Security -> Screen Recording & System Audio
- "Magnet" rights under
    - System Settings -> Privacy & Security -> Accessibility
- "Elgato Camera Hub" rights under
    - System  Settings -> Privacy & Security -> screen Recording & System Audio